﻿using System;
using System.Linq;
using System.IO.Ports;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;

namespace ColorDrawer
{
    class Program
    {
        private static readonly CancellationTokenSource cts = new CancellationTokenSource();
        private static readonly ManualResetEvent programFinished = new ManualResetEvent(false);

        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Console.CancelKeyPress += Console_CancelKeyPress;
            
            var comport = args[0];
            var form = new Form();
            //form.WindowState = FormWindowState.Maximized;
            form.Show();
            using (var serial = new SerialPort(comport))
            {
                serial.BaudRate = 115200;
                serial.ReadTimeout = 300;
                serial.Open();
                Console.WriteLine("Connected!");
                
                while (!cts.Token.IsCancellationRequested)
                {
                    string line;
                    if (!TryReadLine(serial, out line))
                    {
                        continue;
                    }
                    Color color;
                    if (TryParseColor(line, out color))
                    {
                        Console.WriteLine(line);
                        form.BackColor = color;
                    }
                    else
                    {
                        Console.WriteLine($"Error while parsing: [{line}]");
                    }
                }
            }

            programFinished.Set();
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            cts.Cancel();
            programFinished.WaitOne();
        }

        private static bool TryReadLine(SerialPort serial, out string line)
        {
            try
            {
                line = serial.ReadLine();
                return true;
            }
            catch (TimeoutException)
            {
                line = null;
                return false;
            }
        }

        private static bool TryParseColor(string line, out Color color)
        {
            try
            {
                color = default(Color);
                var rgb = line.Split(new[] { ' ' }, 3).Select(int.Parse).ToArray();
                color = Color.FromArgb(rgb[0], rgb[1], rgb[2]);
                return true;
            }
            catch
            {
                color = default(Color);
                return false;
            }
        }
    }
}
